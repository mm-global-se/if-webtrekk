# Webtrekk

---

[TOC]

## Introduction

Register and initialize Webtrekk integration.

## Download

* [webtrekk-register.js](https://bitbucket.org/mm-global-se/if-webtrekk/src/master/src/webtrekk-register.js)

* [webtrekk-initialize.js](https://bitbucket.org/mm-global-se/if-webtrekk/src/master/src/webtrekk-initialize.js)

## Ask client for

+ Campaign name

## Deployment instructions

### Content Campaign

+ Ensure that you have Integration Factory plugin deployed on site level and has _order: -10_ ([find out more](https://bitbucket.org/mm-global-se/integration-factory))

+ Create another site script with _order: -5_ and add the code from [webtrekk-register.js](https://bitbucket.org/mm-global-se/if-webtrekk/src/master/src/webtrekk-register.js) into it

+ Create campaign script with _order: > -5_, then customize the code from [webtrekk-initialize.js](https://bitbucket.org/mm-global-se/if-webtrekk/src/master/src/webtrekk-initialize.js) and add into it

### Redirect Campaign

Same as for Content Campaign + integration initialization script should be mapped to both, generation and redirect, pages.

## QA

+ Open Chrome DevTools

+ Go to 'Network' tab

+ Filter by `webtrekk`

+ Under 'Header' tab find `ct` parameter with campaign inforamtion

![qa](assets/webtrekk.png)
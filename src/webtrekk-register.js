mmcore.IntegrationFactory.register(
  'Webtrekk', {
    
    defaults: {},

    validate: function(data){
        if (!data.campaign)
            return 'No campaign.';
        return true;
    },

    check: function(data){
        return ( window.wt && typeof(window.wt.sendinfo) === 'function' );
    },
    
    exec: function(data){
        var prodSand = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
            testString = prodSand + data.campaignInfo.replace(/[=:|]/g, '_');
      
        window.wt.sendinfo ({ linkId : testString });
        
        if (data.callback) data.callback();
        return true;
    }
});